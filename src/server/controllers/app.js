﻿'use strict';

const router = require('express').Router();

router.get('/', function (request, response) {

    response.render('app/index');

});

module.exports = router;