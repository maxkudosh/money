﻿(function (angular) {

    'use strict';

    const app = angular.module('money.wallet.management');

    app.service('IncomeService', ['$resource', 'UsernameService', function ($resource, usernameService) {

        const currentUserName = usernameService.getCurrentUserName();

        const Income = $resource('/api/user/:user/wallet/:walletId/income/:incomeId/', { user: currentUserName });

        return {

            create: (walletId, model) => Income.save({ walletId: walletId }, model)

        };

    }]);

})(angular);
