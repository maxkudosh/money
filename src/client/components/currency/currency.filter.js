﻿(function (angular) {

    'use strict';

    const app = angular.module('money.currency');

    app.filter('moneyCurrency', ['$filter', 'CurrencyService', function ($filter) {

        return function currencyFilter(amount, currencyCode) {

            const formattedValue = $filter('number')(amount, 2);

            return `${formattedValue}`;

        };

    }]);


})(angular);