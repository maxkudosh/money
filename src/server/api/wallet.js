﻿'use strict';

const walletProvider = require('../models').getWalletProvider();

const IdentifiedApiEndpoint = require('./infrastructure/resourceWithIdEndpoint');

const ID_PARAMETER = 'walletId';

const walletEndpoint = new IdentifiedApiEndpoint(ID_PARAMETER, ['username'], walletProvider);

const routeBinder = require('./infrastructure/resourceWithIdEndpointBinder');

module.exports = routeBinder('/api/user/:username/wallet/', ID_PARAMETER, walletEndpoint);