﻿'use strict';

const NestedCollectionProvider = require('./nestedCollectionProvider');

const WalletProvider = NestedCollectionProvider.bind(null, [user => user.wallets]);

module.exports = WalletProvider;
