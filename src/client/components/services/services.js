﻿(function (angular) {

    'use strict';

    angular.module('money.services', ['ngCookies', 'ngResource']);

})(angular);
