﻿(function (angular) {

    'use strict';

    angular.module('money.navbar', ['ui.router', 'money.routes']);

})(angular);
