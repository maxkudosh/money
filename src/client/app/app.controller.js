﻿(function (angular) {

    'use strict';

    function AppController(usernameProvider) {

        this.username = usernameProvider.getCurrentUserName();

    }

    AppController.$inject = ['UsernameService'];

    angular
        .module('money')
        .controller('AppController', AppController);

})(angular);
