﻿(function (angular) {

    'use strict';

    angular
        .module('money.wallet.balance', [
            'ui.router',
            'money.routes',
            'money.currency',
            'money.services',
            'money.sorters',
            'money.directives'
        ])
        .config(configureRoutes);

    function configureRoutes($stateProvider, ROUTES) {

        $stateProvider
            .state(ROUTES.walletBalance.stateName, {
                url: ROUTES.walletBalance.path,
                templateUrl: 'app/wallet/balance/wallet-balance.html'
            });

    }

    configureRoutes.$inject = ['$stateProvider', 'ROUTES'];

})(angular);
