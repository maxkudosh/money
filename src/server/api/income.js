﻿'use strict';

const incomeProvider = require('../models').getIncomeProvider();

const BaseApiEndpoint = require('./infrastructure/resourceEndpoint');

const expenseEndpoint = new BaseApiEndpoint(['username', 'walletId'], incomeProvider);

const routeBinder = require('./infrastructure/resourceEndpointBinder');

module.exports = routeBinder('/api/user/:username/wallet/:walletId/income/', expenseEndpoint);