'use strict';

var strings = require('../config/resources').strings;

var cryptoHelper = require('../components/cryptoHelper');

function UserService(userModel) {

    this.user = userModel;

}

UserService.prototype.getById = function (id, done) {

    this.user.getById(id, function (error, user) {

        if (error) {
            return done(error);
        }

        if (!user) {
            return done(null, false, strings.noSuchUser);
        }

        return done(null, user);

    });

};

UserService.prototype.signUpNewUser = function(name, password, done) {

    var self = this;

    self.user.getByName(name, function(error, user) {

        if (error) {
            return done(error);
        }

        if (user) {
            return done(null, false, strings.usernameIsTaken);
        }

        var salt = cryptoHelper.generateSalt();
        var hash = cryptoHelper.encrypt(password, salt);

        return self.user.create(name, salt, hash, done);

    });

};

UserService.prototype.checkCredentials = function(name, password, done) {

    this.user.getByName(name, function(error, user) {

        if (error) {
            return done(error);
        }
        
        if (!user || !cryptoHelper.doesPasswordMatchHash(password, user.hash, user.salt)) {
            return done(null, false, strings.wrongPassword);
        }

        return done(null, user);

    });

};

module.exports = UserService;
