﻿(function (angular) {

    'use strict';

    angular
        .module('money.wallet', [
            'ui.router',
            'money.routes',
            'money.currency',
            'money.services',
            'money.wallet.management',
            'money.wallet.balance',
            'money.wallet.new'
        ])
        .config(configureRoutes);

    function configureRoutes($stateProvider, ROUTES) {

        $stateProvider
            .state(ROUTES.myWallets.stateName, {
                url: ROUTES.myWallets.path,
                templateUrl: 'app/wallet/wallets.html'
            });

    }

    configureRoutes.$inject = ['$stateProvider', 'ROUTES'];

})(angular);
