'use strict';

var express = require('express');
var favicon = require('serve-favicon');
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var path = require('path');
var config = require('./environment');
var session = require('express-session');
var passport = require('passport');

module.exports = function (app) {

    var env = app.get('env');

    app.set('views', config.root + '/server/views');
    app.set('view engine', 'jade');
    app.use(bodyParser.urlencoded({ extended: false }));
    app.use(bodyParser.json());
    app.use(cookieParser());
    app.use(session({ secret: config.sessionSecret}));
    app.use(passport.initialize());
    app.use(passport.session());
    app.use(express.static(path.join(config.root, '/client/')));

};
