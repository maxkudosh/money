﻿(function (angular) {

    'use strict';

    function DateHelper() {}

    DateHelper.prototype.getDatesBetween = function (startDate, endDate, inclusion) {

        let dates = [];

        let currentDate = new Date(startDate);

        const notIncludeStartDate = inclusion && !inclusion.startDate;

        if (notIncludeStartDate) {
            currentDate = currentDate.setDate(currentDate.getDate() + 1);
        }

        const notIncludeEndDate = inclusion && !inclusion.endDate;

        if (notIncludeEndDate) {
            endDate = endDate.setDate(endDate.getDate() - 1);
        }

        while (currentDate <= endDate) {

            dates.push(new Date(currentDate));

            currentDate.setDate(currentDate.getDate() + 1);

        }

        return dates;

    };

    DateHelper.prototype.sameDay = function (first, second) {

        return first.getDate() === second.getDate() &&
               first.getMonth() === second.getMonth() &&
               first.getYear() === second.getYear();

    };

    DateHelper.prototype.isInBetween = function (date, start, end) {

        return (start < date || this.sameDay(start, date)) && (date < end || this.sameDay(date, end));

    };

    DateHelper.prototype.oneMonthBackInTime = function () {

        let monthAgo = new Date();
        monthAgo.setMonth(monthAgo.getMonth() - 1);
        return monthAgo;

    };

    angular
        .module('money.dates', [])
        .service('DateHelper', DateHelper);

})(angular);
