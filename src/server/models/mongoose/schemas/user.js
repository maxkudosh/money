'use strict';

const mongoose = require('mongoose');

const WalletSchema = require('./wallet');
const TagSchema = require('./tag');

const UserSchema = new mongoose.Schema({
    name: String,
    salt: String,
    hash: String,
    wallets: [
        WalletSchema
    ],
    tags: [
        TagSchema
     ]
});

UserSchema.statics.findByName = function (name, done) {
    return this.findOne({ name: name }, done);
}; 

module.exports = UserSchema;
