﻿(function (angular) {

    'use strict';

    const app = angular.module('money.wallet.management');

    app.service('ExpenseService', ['$resource','UsernameService', function ($resource, usernameService) {

        const currentUserName = usernameService.getCurrentUserName();

        const Expense = $resource('/api/user/:user/wallet/:walletId/expense/:expenseId/', { user: currentUserName });

        return {

            create: (walletId, model) => Expense.save({ walletId: walletId }, model)

        };


    }]);

}) (angular);
