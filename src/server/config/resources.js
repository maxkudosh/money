﻿module.exports = {

    strings: {
        wrongPassword: 'Wrong password or username!',
        noSuchUser: 'There is no user with such username!',
        needToSignUp: 'You need to sign up first!',
        usernameIsTaken: 'The username is already in use, try another!',
        genericErrorMessage: 'Something went wrong... Try again!'
    },

};
