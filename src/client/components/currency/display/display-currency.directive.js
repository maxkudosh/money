﻿(function (angular) {

    'use strict';

    const BALANCE_CLASSES = {

        positive: 'alert-success',
        negative: 'alert-danger',
        zero: 'alert-warning'

    };

    function determineBalanceClass(amount) {

        if (amount > 0) {
            return BALANCE_CLASSES.positive;
        }

        if (amount < 0) {
            return BALANCE_CLASSES.negative;
        }

        return BALANCE_CLASSES.zero;

    }

    function displayCurrency() {

        return {

            restrict: 'E',
            templateUrl: 'components/currency/display/display-currency.html',
            scope: {
                amountOfMoney: '@amount',
                currencyCode: '@currencyCode'
            },
            link: scope => {

                scope.getBalanceClass = determineBalanceClass;

            }

        };

    }

    displayCurrency.$inject = [];

    angular
        .module('money.currency')
        .directive('displayCurrency', displayCurrency);

})(angular);
