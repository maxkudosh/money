﻿(function (angular) {

    'use strict';

    const BalanceChangeTypes = {
        INCOME: {
            id: 0,
            name: 'Income'
        },
        EXPENSE: {
            id: 1,
            name: 'Expense'
        }
    };

    angular
        .module('money.wallet.management')
        .constant('BalanceChangeTypes', BalanceChangeTypes);

})(angular);
