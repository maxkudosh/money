﻿(function (angular) {

    'use strict';

    angular
        .module('money.currency.icons')
        .directive('currencyCodeIcon', currencyCodeIcon);

    function currencyCodeIcon(CURRENCY_ICON_CLASSES) {

        return {

            restrict: 'E',
            templateUrl: 'components/currency/currency-code-icons/currency-code-icon.html',
            scope: {
                currencyCode: '@'
            },
            link: function (scope, element) {

                scope.getIcon = currencyCode => CURRENCY_ICON_CLASSES[scope.currencyCode];

            }
        }

    }

    currencyCodeIcon.$inject = ['CURRENCY_ICONS'];

})(angular);