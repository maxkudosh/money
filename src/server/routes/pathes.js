﻿'use strict'

var pathes = {

    account: {
        signin: '/account/signin',
        signup: '/account/signup',
        signout: '/account/signout'
    },

    app: '/',

    api: '/api'

}

module.exports = pathes;
