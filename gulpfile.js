﻿var gulp = require('gulp');
var bundle = require('gulp-bundle-assets');
var templateCache = require('gulp-angular-templatecache');
var merge = require('merge-stream');
var babel = require('gulp-babel');
var clean = require('gulp-clean');

var destination = './build/';
var babelDestination = `${destination}_babeled/`;
var clientDestination = `${destination}client/`;

gulp.task('clean', function () {

    return gulp
        .src(destination, { read: false })
        .pipe(clean());

});

gulp.task('babel', ['clean'], function () {
    return gulp
        .src(['./src/**/*.js',])
        .pipe(babel())
        .pipe(gulp.dest(babelDestination));
});

gulp.task('server', ['clean', 'babel'], function () {

    var serverFilesDestination = destination + 'server/';

    var views = gulp
        .src('src/server/**/*.jade')
        .pipe(gulp.dest(serverFilesDestination));

    var scripts = gulp
        .src([babelDestination + 'server/**/*.js'])
        .pipe(gulp.dest(serverFilesDestination));

    return merge(views, scripts);

});

gulp.task('clean-temp', ['babel', 'server', 'client'], function () {
    return gulp
        .src(babelDestination, { read: false })
        .pipe(clean());
});

gulp.task('client', ['clean', 'babel'], function () {
    return gulp
        .src('./bundle.config.js')
        .pipe(bundle())
        .pipe(gulp.dest(clientDestination));
});

gulp.task('template', ['clean'], function () {
    return gulp
        .src(['./src/client/**/*.html'])
        .pipe(templateCache({
            moduleSystem: 'IIFE',
            module: 'money.templates',
            standalone: true
        }))
        .pipe(gulp.dest(clientDestination));
});

gulp.task('default', ['clean', 'babel', 'client', 'template', 'server', 'clean-temp']);
