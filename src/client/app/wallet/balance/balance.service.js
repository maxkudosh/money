﻿(function (angular) {

    'use strict';

    function WalletBalanceService(walletService, descendingSorter) {

        this.walletService = walletService;
        this.descendingSorter = descendingSorter;

    }

    WalletBalanceService.prototype.getWalletBalance = function (walletId) {

        const walletPromise = this.walletService.get(walletId);

        walletPromise.$promise.then(wallet => {

            const negatedExpenses = wallet.expenses.map(negateAmount);

            const incomesAndExpenses = wallet.incomes.concat(negatedExpenses);

            const orderedIncomesAndExpenses = incomesAndExpenses.sort(this.descendingSorter(i => i.whenHappened));

            walletPromise.incomesAndExpenses = orderedIncomesAndExpenses;

            return walletPromise;

        });

        return walletPromise;

    };

    function negateAmount(object) {
        return Object.assign({}, object, { amount: - object.amount });
    }

    WalletBalanceService.$inject = ['WalletService', 'DescendingSorterFactory'];

    angular
        .module('money.wallet.balance')
        .service('BalanceService', WalletBalanceService);

}) (angular);
