﻿'use strict';

function sum(...operands) {

    return operands.reduce((a, b) => a + b, 0);

}

function sumByProperty(property, array) {

    return sum(...array.map(property));

}

module.exports = {

    sum: sum,
    sumByProperty: sumByProperty

};
