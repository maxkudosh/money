﻿'use strict';

const express = require('express');

function bindRoute(routeToBind, endpoint) {

    const router = express.Router();

    router.route(routeToBind)
        .get(endpoint.getAll.bind(endpoint))
        .post(endpoint.post.bind(endpoint));

    return router;

}

module.exports = bindRoute;