﻿(function (angular) {

    'use strict';

    function StatsController(statsService, walletsService, dateHelper) {

        this.statsService = statsService;
        this.walletsService = walletsService;
        this.dateHelper = dateHelper;

        this.activate();

    }

    StatsController.prototype.activate = function () {

        this.showStats = false;

        this.startDate = this.dateHelper.oneMonthBackInTime();

        this.endDate = new Date();

        this.maxDate = new Date();

        this.chosenWalletId = null;

        this.wallets = this.walletsService.getAll();

    };

    StatsController.prototype.refreshStats = function () {

        if (this.chosenWalletId) {
            this.stats = this.statsService.getStatsForWallet(this.chosenWalletId, this.startDate, this.endDate);
            this.showStats = true;
        } else {
            this.showStats = false;
        }

    };

    StatsController.$inject = ['StatsService', 'WalletService', 'DateHelper'];

    angular
        .module('money.stats')
        .controller('StatsController', StatsController);

})(angular);
