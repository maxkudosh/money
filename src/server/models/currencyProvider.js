﻿'use strict';

const CURRENCIES = [
    {
        code: 'USD',
        name: 'US DOLLAR'
    },
    {
        code: 'BTC',
        name: 'BITCOIN'
    }
];

class CurrencyProvider {

    getAll(done) {

        return done(null, CURRENCIES);

    }

}

module.exports = CurrencyProvider;
