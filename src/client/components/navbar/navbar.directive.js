﻿(function (angular) {

    'use strict';

    const app = angular.module('money.navbar');

    app.directive('appNavigationBar', function () {

        return {

            restrict: 'A',
            templateUrl: 'components/navbar/navbar.html'

        };

    });

})(angular);
