﻿'use strict';

const tagProvider = require('../models').getTagProvider();

const BaseApiEndpoint = require('./infrastructure/resourceEndpoint');

const tagEndpoint = new BaseApiEndpoint(['username'], tagProvider);

const routeBinder = require('./infrastructure/resourceEndpointBinder');

module.exports = routeBinder('/api/user/:username/tag/', tagEndpoint);