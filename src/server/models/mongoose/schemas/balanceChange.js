﻿'use strict';

const mongoose = require('mongoose');

const TagSchema = require('./tag');

const moneyMovement = {
    name: String,
    description: String,
    tags: [TagSchema],
    amount: Number,
    whenHappened: {
        type: Date,
        default: Date.now
    }

};

module.exports = moneyMovement;
