﻿'use strict';

const mongoose = require('mongoose');

const sumByProperty = require('../../../components/functions').sumByProperty;

const ExpenseSchema = require('./expense');
const IncomeSchema = require('./income');

const WalletSchema = new mongoose.Schema({
    name: String,
    currencyCode: String,
    description: String,
    creationDate: {
        type: Date,
        default: Date.now
    },
    expenses: [
        ExpenseSchema
    ],
    incomes: [
        IncomeSchema
    ]
});

WalletSchema.set('toObject', { virtuals: true });
WalletSchema.set('toJSON', { virtuals: true });

WalletSchema.virtual('balance').get(function () {

    var expenseSummary = sumByProperty(e => e.amount, this.expenses);

    var incomeSummary = sumByProperty(i => i.amount, this.incomes);

    return incomeSummary - expenseSummary;

});

module.exports = WalletSchema;
