﻿(function (angular) {

    'use strict';

    function WalletsController(stateManager, routes, walletService) {

        this.routes = routes;
        this.stateManager = stateManager;
        this.walletService = walletService;

        this.wallets = this.walletService.getAll();

        this.anyWallets = () => this.wallets.length > 0;

        this.hasDescription = wallet => !!wallet.description;

        this.allowAddNewWallet = () => this.stateManager.current.name === this.routes.myWallets.stateName;

        this.isWalletViewActive = wallet => this.stateManager.params.walletId === wallet.id;

        this.showBalance = goTo(this.routes.walletBalance);

        this.addExpense = goTo(this.routes.newExpense);

        this.addIncome = goTo(this.routes.newIncome);

        this.remove = walletToRemove =>
            this.walletService
                .delete(walletToRemove.id).$promise
                .then(() => this.wallets = this.wallets.filter(w => w.id !== walletToRemove.id))
                .then(() => {
                    if (this.isWalletViewActive(walletToRemove)) {
                        this.stateManager.go(this.routes.myWallets.stateName);
                    }
                });

    }

    function goTo(route) {

        return function (wallet) {
            this.stateManager.go(route.stateName, { walletId: wallet.id });
        };
    }

    WalletsController.$inject = ['$state', 'ROUTES', 'WalletService'];

    angular
        .module('money.wallet')
        .controller('WalletsController', WalletsController);

})(angular);
