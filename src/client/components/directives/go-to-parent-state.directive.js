﻿(function (angular) {

    'use strict';

    angular
        .module('money.directives')
        .directive('toParentState', toParentState);


    function toParentState($state) {

        function link(scope, element) {

            element.on('click', () => $state.go('^'));

        }

        return {
            restrict: 'A',
            link: link
        };

    }

    toParentState.$inject = ['$state'];

})(angular);
