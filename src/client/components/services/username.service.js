﻿(function (angular) {

    'use strict';

    const app = angular.module('money.services');

    app.service('UsernameService', ['$cookies', function ($cookies) {

        this.getCurrentUserName = function () {

            if (!this.cachedUsername) {
                this.cachedUsername = $cookies.get('money-app-username');
            }

            return this.cachedUsername;

        };

    }]);

})(angular);
