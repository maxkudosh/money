﻿(function (angular) {

    'use strict';

    function BalanceController(stateParams, balanceService) {

        this.stateParams = stateParams;
        this.balanceService = balanceService;

        this.activate();

    }

    BalanceController.prototype.activate = function () {

        this.wallet = this.balanceService.getWalletBalance(this.stateParams.walletId);

    };

    BalanceController.$inject = ['$stateParams', 'BalanceService'];

    angular
        .module('money.wallet.balance')
        .controller('BalanceController', BalanceController);

})(angular);
