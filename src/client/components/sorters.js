﻿(function (angular) {

    'use strict';

    const Order = {

        A_COMES_FIRST: -1,
        B_COMES_FIRST: 1,
        DOESNT_MATTER: 0

    };

    function sortDescending(a, b) {
        if (a > b) return Order.A_COMES_FIRST;
        if (a < b) return Order.B_COMES_FIRST;
        return Order.DOESNT_MATTER;
    }

    function DescendingSorter(propertyGetter) {

        return (a, b) => sortDescending(propertyGetter(a), propertyGetter(b));

    }

    angular
        .module('money.sorters', [])
        .factory('DescendingSorterFactory', () => DescendingSorter);

})(angular);
