﻿'use strict';

const NestedCollectionProvider = require('./nestedCollectionProvider');

const IncomeProvider = NestedCollectionProvider.bind(null, [user => user.wallets, wallet => wallet.incomes]);

module.exports = IncomeProvider;
