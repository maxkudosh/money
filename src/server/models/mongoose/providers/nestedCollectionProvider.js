﻿'use strict';

function getNestedCollection(user, getters, ids) {

    let currentNestedCollection = getters[0](user);

    for (let i = 0; i < getters.length - 1 && i < ids.length; i++) {

        currentNestedCollection = getters[i + 1](currentNestedCollection.id(ids[i]));

    }

    return currentNestedCollection;

}

class NestedCollectionProvider {

    constructor(gettersChain, userModel) {

        if (gettersChain.length === 0) {
            throw new Error('Chain of getters of nested documents must not be empty');
        }

        this.chain = gettersChain;
        this.userModel = userModel;

    }

    getAll(done, username, ...nestedIds) {

        if (nestedIds.length !== this.chain.length - 1) {
            throw new Error('Number of nested ids must be one less then number of nested getters');
        }

        return this.userModel.findByName(username, (error, user) => {

            return done(error, getNestedCollection(user, this.chain, nestedIds));

        });

    }

    delete(done, username, ...nestedIds) {

        return this.userModel.findByName(username, (error, user) => {

            const nestedCollection = getNestedCollection(user, this.chain, nestedIds);

            nestedCollection.id(nestedIds[nestedIds.length - 1]).remove();

            return user.save((error) => done(error));

        });

    }

    getById(done, username, ...nestedIds) {

        return this.userModel.findByName(username, (error, user) => {

            const nestedCollection = getNestedCollection(user, this.chain, nestedIds);

            return done(error, nestedCollection.id(nestedIds[nestedIds.length - 1]));

        });

    }

    create(model, done, username, ...nestedIds) {

        return this.userModel.findByName(username, (error, user) => {

            const nestedCollection = getNestedCollection(user, this.chain, nestedIds);

            const newModel = nestedCollection.create(model);

            nestedCollection.push(newModel);

            return user.save(error => done(error, newModel));

        });

    }

}

module.exports = NestedCollectionProvider;
