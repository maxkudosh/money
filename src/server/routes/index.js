﻿'use strict';

var strings = require('../config/resources').strings;

var pathes = require('./pathes');

function authMiddleware(request, response, next) {

    if (request.isAuthenticated()) {
        return next();
    }

    var isAjaxRequest = request.xhr;
    
    if (isAjaxRequest) {
        
        var UNAUTHORIZED_STATUS_CODE = 403;
        
        return response.status(UNAUTHORIZED_STATUS_CODE).send({
            success: false,
            message: strings.needToSignUp
        });

    }

    response.redirect(pathes.account.signin);

}

module.exports = function (app) {

    var accountController = require('../controllers/account');
    app.use(accountController);

    var appController = require('../controllers/app');
    app.use(authMiddleware, appController);

    var walletApi = require('../api/wallet');
    app.use(authMiddleware, walletApi);

    var currencyApi = require('../api/currency');
    app.use(authMiddleware, currencyApi);

    var expenseApi = require('../api/expense');
    app.use(authMiddleware, expenseApi);

    var incomeApi = require('../api/income');
    app.use(authMiddleware, incomeApi);

    var tagApi = require('../api/tag');
    app.use(authMiddleware, tagApi);


};
