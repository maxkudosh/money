﻿'use strict';

const mongoose = require('mongoose');

const TagSchema = require('./mongoose/schemas/tag');

const UserSchema = require('./mongoose/schemas/user');

const UserProvider = require('./mongoose/providers/userProvider');

const TagProvider = require('./mongoose/providers/tagProvider');

const WalletProvider = require('./mongoose/providers/walletProvider');

const ExpenseProvider = require('./mongoose/providers/expenseProvider');

const IncomeProvider = require('./mongoose/providers/incomeProvider');

const UserModel = mongoose.model('user', UserSchema);

const CurrencyProvider = require('./currencyProvider');

module.exports = {

    getUserProvider: () => new UserProvider(UserModel),

    getWalletProvider: () => new WalletProvider(UserModel),

    getExpenseProvider: () => new ExpenseProvider(UserModel),

    getIncomeProvider: () => new IncomeProvider(UserModel),

    getTagProvider: () => new TagProvider(UserModel),

    getCurrencyProvider: () => new CurrencyProvider()

};
