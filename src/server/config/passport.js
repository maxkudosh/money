﻿'use strict';

var passport = require('passport');

var LocalStrategy = require('passport-local').Strategy;

var modelFactory = require('../models/');

var user = modelFactory.getUserProvider();

var UserService = require('../services/userService');

var userService = new UserService(user);


module.exports = function (app) {

    passport.use(new LocalStrategy(function (name, password, done) {

        return userService.checkCredentials(name, password, done);

    }));

    passport.serializeUser(function (user, done) {
        return done(null, user._id.toString());
    });

    passport.deserializeUser(function (id, done) {
        return userService.getById(id, done);
    });

};
