﻿(function (angular) {

    'use strict';

    function NewWalletController(stateManager, walletService, currencyService) {

        this.stateManager = stateManager;
        this.walletService = walletService;
        this.currencyService = currencyService;

        this.activate();

    }

    NewWalletController.prototype.activate = function () {

        this.currencies = this.currencyService.getAllCurrencies();

    };

    NewWalletController.prototype.submit = function () {

        const wallet = {
            name: this.name,
            currencyCode: this.currencyCode,
            description: this.description,
            creationDate: Date.now
        };

        this.walletService
            .create(wallet).$promise
            .then(() => this.stateManager.go('^', {}, { reload: true }));

    };

    NewWalletController.$inject = ['$state', 'WalletService', 'CurrencyService'];

    angular
        .module('money.wallet.new')
        .controller('NewWalletController', NewWalletController);

})(angular);
