﻿Actually this is a third party multiselect control.
See here: https://github.com/isteven/angular-multi-select
It is not included in the project as bower dependency because
it was necessary to extend it's functionality
(to allow adding new item to multiselect items via UI, to be exact).
Storing it as a bower dependency would lead to bugs and errors for those
who clone the project and do bower install.

I'm aware that this is not best practice.