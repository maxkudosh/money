﻿'use strict';

const mongoose = require('mongoose');

const baseBalanceChangeSchema = require('./balanceChange');

const ExpenseSchema = new mongoose.Schema(
    baseBalanceChangeSchema
);

module.exports = ExpenseSchema;
