﻿(function (angular) {

    'use strict';

    class CurrencyService {

        constructor(restHelper) {

            // Need to enable cache for currencies
            this.currencyWrapper = restHelper('/api/currency/', {}, { query: { method: 'GET', cache: true, isArray: true} }, {});

        }

        getAllCurrencies() {

            return this.currencyWrapper.query();

        }

    }

    const app = angular.module('money.currency');

    app.service('CurrencyService', [
        '$resource',
        $resource => new CurrencyService($resource)
    ]);

})(angular);
