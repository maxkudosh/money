﻿# Environment

- Mongo 3+
- Node 5+

# Steps to run the project locally:

- npm install
- bower install
- gulp
- node build/server/app.js
