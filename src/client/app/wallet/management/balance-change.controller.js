﻿(function (angular) {

    'use strict';

    function BalanceChangeController(stateManager, walletService, tagService, balanceChangeService, type) {

        this.type = type;

        this.stateManager = stateManager;
        this.balanceChangeService = balanceChangeService;
        this.tagService = tagService;
        this.walletService = walletService;

        this.activate();

    }

    BalanceChangeController.prototype.activate = function () {

        this.balanceChange = this.getDefaults();
        this.wallet = this.walletService.get(this.stateManager.params.walletId);
        this.tags = this.tagService.getAll();

    };

    BalanceChangeController.prototype.getDefaults = function () {

        return {
            tags: [],
            when: Date.now()
        };

    };

    BalanceChangeController.prototype.saveNewTag = function(data) {
        const newTag = { name: data.keyword };
        this.tagService.create(newTag);
        return newTag;
    }


    BalanceChangeController.prototype.submit = function() {

        const newBalanceChange = {
            name: this.balanceChange.name,
            description: this.balanceChange.description,
            amount: +this.balanceChange.amount,
            whenHappened: this.balanceChange.when,
            tags: this.balanceChange.tags
        };

        this.balanceChangeService
            .create(this.wallet.id, newBalanceChange).$promise
            .then(() => this.stateManager.go('^', {}, { reload: true }));

    }


    BalanceChangeController.$inject = ['$state', 'WalletService', 'TagService', 'BalanceChangeService', 'type'];

    angular
        .module('money.wallet.management')
        .controller('BalanceChangeController', BalanceChangeController);

})(angular);
