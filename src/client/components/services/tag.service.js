﻿(function (angular) {

    'use strict';

    function TagService(restApiWrapper, usernameService) {

        const api = restApiWrapper('/api/user/:user/tag/', { user: usernameService.getCurrentUserName() });

        this.getAll = () => api.query();
        this.create = (newTag) => api.save(newTag);

    }

    TagService.$inject = ['$resource', 'UsernameService'];

    angular
        .module('money.services')
        .service('TagService', TagService);

})(angular);
