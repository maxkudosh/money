(function (angular) {

    'use strict';

    class NavbarController {

        constructor($state, ROUTES) {

            this.stateAccessor = $state;

            this.menu = [
                ROUTES.myWallets,
                ROUTES.statistics
            ];

        }

        isStateActive(stateName) {

            return this.stateAccessor.current.name.indexOf(stateName) >= 0;

        }

    }

    const app = angular.module('money.navbar');

    app.controller('NavbarController', ['$state', 'ROUTES', function ($state, ROUTES) {

        return new NavbarController($state, ROUTES);

    }]);

})(angular);
