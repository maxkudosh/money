﻿'use strict';

const express = require('express');

class ResourceEndpoint {

    constructor(orderedRouteParameters, provider) {
        this.provider = provider;
        this.orderedRouteParameters = orderedRouteParameters;
    }

    getRouteValues(request) {

        return this.orderedRouteParameters.map(parameterName => request.params[parameterName]);

    }

    getAll(req, res) {

        const routeParamsValues = this.getRouteValues(req);

        return this.provider.getAll.call(this.provider, (error, data) => res.status(200).json(data), ...routeParamsValues);

    }

    post(req, res) {

        const routeParamsValues = this.getRouteValues(req);

        const data = req.body;

        return this.provider.create.call(this.provider, data, (error, savedData) => {

            res.location(req.route.path + savedData.id);

            return res.sendStatus(201);

        }, ...routeParamsValues);

    }

}

module.exports = ResourceEndpoint;