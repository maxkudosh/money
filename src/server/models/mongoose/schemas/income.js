﻿'use strict';

const mongoose = require('mongoose');

const baseBalanceChangeSchema = require('./balanceChange');

const IncomeSchema = new mongoose.Schema(
    baseBalanceChangeSchema
);

module.exports = IncomeSchema;
