﻿(function (angular) {

    'use strict';

    function map(wallet) {

        return {

            name: wallet.name,
            creationDate: new Date(wallet.creationDate),
            description: wallet.description,
            currencyCode: wallet.currencyCode,

            incomes: wallet.incomes.map(i => Object.assign({}, i, { whenHappened: new Date(i.whenHappened) })),
            expenses: wallet.expenses.map(e => Object.assign({}, e, { whenHappened: new Date(e.whenHappened) })),

            balance: wallet.balance

        };

    }

    function mapMany(wallets) {
        return wallets.map(map);
    }

    function WalletService(restApiWrapper, usernameService) {

        const route = '/api/user/:user/wallet/:walletId';

        const defaults = {
            user: usernameService.getCurrentUserName()
        };

        const actionsConfig = {
            query: { method: 'GET', isArray: true, interceptor: { response: r => mapMany(r.data) } },
            get: { method: 'GET', interceptor: { response: r => map(r.data) } }
        };

        this.api = restApiWrapper(route, defaults, actionsConfig);

    }

    WalletService.$inject = ['$resource', 'UsernameService'];

    WalletService.prototype.getAll = function () {
        return this.api.query()
    };

    WalletService.prototype.get = function (id) {
        return this.api.get({ walletId: id });
    };

    WalletService.prototype.create = function (model) {
        return this.api.save(model);
    };

    WalletService.prototype.delete = function (id) {
        return this.api.delete({ walletId: id });
    };

    angular
        .module('money.services')
        .service('WalletService', WalletService);

})(angular);
