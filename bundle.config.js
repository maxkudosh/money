﻿const resultTypes = {
    type: {
        scripts: 'plain',
        styles: 'plain'
    }
};

module.exports = {
    bundle: {
        vendor: {
            scripts: [
                {
                    src: './bower_components/chart.js/chart.js',
                    minSrc: './bower_components/chart.js/chart.min.js'
                },
                {
                    src: './bower_components/angular/angular.js',
                    minSrc: './bower_components/angular/angular.min.js'
                },
                {
                    src: './bower_components/angular-cookies/angular-cookies.js',
                    minSrc: './bower_components/angular-cookies/angular-cookies.min.js'
                },
                {
                    src: './bower_components/angular-resource/angular-resource.js',
                    minSrc: './bower_components/angular-resource/angular-resource.min.js'
                },
                {
                    src: './bower_components/angular-bootstrap/ui-bootstrap-tpls.js',
                    minSrc: './bower_components/angular-bootstrap/ui-bootstrap-tpls.min.js'
                },
                {
                    src: './bower_components/angular-ui-router/release/angular-ui-router.js',
                    minSrc: './bower_components/angular-ui-router/release/angular-ui-router.min.js'
                },
                {
                    src: './bower_components/angular-chart.js/dist/angular-chart.js',
                    minSrc: './bower_components/angular-chart.js/dist/angular-chart.min.js'
                }
            ],
            styles: [
                {
                    src: './bower_components/bootstrap/dist/css/bootstrap.css',
                    minSrc: './bower_components/bootstrap/dist/css/bootstrap.min.css'
                },
                {
                    src: './bower_components/font-awesome/css/font-awesome.css',
                    minSrc: './bower_components/font-awesome/css/font-awesome.min.css'
                },
                {
                    src: './bower_components/angular-chart.js/dist/angular-chart.css',
                    minSrc: './bower_components/angular-chart.js/dist/angular-chart.min.css'
                }
            ],
            options: {
                useMin: 'production',
                uglify: false,
                minCss: false,
                rev: false,
                result: resultTypes
            }
        },
        app: {
            scripts: ['./build/_babeled/client/**/*.js'],
            styles: ['./src/client/**/*.css'],
            options: {
                uglify: 'production',
                rev: 'production',
                minCss: 'production',
                order: {
                    scripts: [
                        '!**/*.@(controller|service|factory|directive|filter|enum).js',
                        '**/*.@(controller|service|factory|directive|filter|enum).js',
                    ]
                },
                result: resultTypes
            }
        }
    },
    copy: [
        {
            src: './bower_components/font-awesome/fonts/*.*',
            base: './bower_components/font-awesome/'
        },
        {
            src: './bower_components/bootstrap/fonts/*.*',
            base: './bower_components/bootstrap/'
        }
        
    ]
};
