﻿(function (angular) {

    'use strict';

    angular
        .module('money.wallet.new', [
            'ui.router',
            'money.routes',
            'money.services',
            'money.currency',
            'money.directives'
        ])
        .config(configureRoutes);

    function configureRoutes($stateProvider, ROUTES) {

        $stateProvider.state(ROUTES.newWallet.stateName, {
            url: ROUTES.newWallet.path,
            templateUrl: 'app/wallet/new/new-wallet.html'
        });

    }

    configureRoutes.$inject = ['$stateProvider', 'ROUTES'];

})(angular);
