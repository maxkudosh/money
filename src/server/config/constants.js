﻿'use strict'

module.exports = {

    minPasswordLength: 4,
    maxPasswordLength: 64,
    usernameCookieName: 'money-app-username'

};