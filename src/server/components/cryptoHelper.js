﻿'use strict';

const cryptoHelper = require('crypto');

// Hash encoding
const ENCODING = 'base64';

// Salt length in bytes
const SALT_LENGTH = 64;
// Desired hash length in bytes
const HASH_LENGTH = 256;
// Hashing algorythm iterations count
const ITERATIONS_COUNT = 10000;

function toString(buffer) {

    return buffer.toString(ENCODING);

}

function generateSalt() {

    return toString(cryptoHelper.randomBytes(16));

}

function getBytes(s){

    return new Buffer(s, ENCODING);

}

function encrypt(password, salt) {

    return toString(cryptoHelper.pbkdf2Sync(password, salt, ITERATIONS_COUNT, HASH_LENGTH));

}

function doesPasswordMatchHash(password, hash, salt) {

    var passwordHash = encrypt(password, salt);

    return toString(passwordHash) === hash;

}

module.exports = {

    doesPasswordMatchHash: doesPasswordMatchHash,
    generateSalt: generateSalt,
    encrypt: encrypt

};
