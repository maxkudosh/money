﻿(function (angular) {

    'use strict';

    angular
        .module('money.wallet.management', [
            'ngResource',
            'ui.router',
            'ui.bootstrap',
            'isteven-multi-select',
            'money.routes',
            'money.currency.icons',
            'money.services',
            'money.directives'
        ])
        .config(configureRoutes);

    function configureRoutes($stateProvider, ROUTES, BalanceChangeTypes) {

        $stateProvider
            .state(ROUTES.newIncome.stateName, {
                url: ROUTES.newIncome.path,
                controller: 'BalanceChangeController',
                controllerAs: 'controller',
                templateUrl: 'app/wallet/management/balance-change.html',
                resolve: {
                    type: () => BalanceChangeTypes.INCOME,
                    BalanceChangeService: 'IncomeService'
                }
            })
            .state(ROUTES.newExpense.stateName, {
                url: ROUTES.newExpense.path,
                controller: 'BalanceChangeController',
                controllerAs: 'controller',
                templateUrl: 'app/wallet/management/balance-change.html',
                resolve: {
                    type: () => BalanceChangeTypes.EXPENSE,
                    BalanceChangeService: 'ExpenseService'
                }
            });

    }

    configureRoutes.$inject = ['$stateProvider', 'ROUTES', 'BalanceChangeTypes'];

}) (angular);
