﻿(function (angular) {

    'use strict';

    function configureRoutes($stateProvider, ROUTES) {

        $stateProvider
            .state(ROUTES.statistics.stateName, {
                url: ROUTES.statistics.path,
                templateUrl: 'app/stats/stats.html'
            });

    }

    configureRoutes.$inject = ['$stateProvider', 'ROUTES'];

    angular
        .module('money.stats', [
            'ui.router',
            'ui.bootstrap',
            'chart.js',
            'money.routes',
            'money.currency',
            'money.dates',
            'money.directives'
        ])
        .config(configureRoutes);

})(angular);
