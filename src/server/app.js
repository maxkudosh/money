'use strict';

process.env.NODE_ENV = 'development';

var express = require('express');
var mongoose = require('mongoose');
var config = require('./config/environment');

mongoose.connect(config.mongo.uri, config.mongo.options);

if(config.seedDB) { require('./config/seed'); }

var app = express();

var constants = require('./config/constants');
app.locals.constants = constants;

var server = require('http').createServer(app);
require('./config/express')(app);
require('./config/passport')(app);
require('./routes')(app);

server.listen(config.port, config.ip);

exports = module.exports = app;
