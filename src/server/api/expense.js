﻿'use strict';

const expenseProvider = require('../models').getExpenseProvider();

const BaseApiEndpoint = require('./infrastructure/resourceEndpoint');

const expenseEndpoint = new BaseApiEndpoint(['username', 'walletId'], expenseProvider);

const routeBinder = require('./infrastructure/resourceEndpointBinder');

module.exports = routeBinder('/api/user/:username/wallet/:walletId/expense/', expenseEndpoint);