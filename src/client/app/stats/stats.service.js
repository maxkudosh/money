﻿(function (angular) {

    'use strict';

    function StatsService(walletService, tagService, dateHelper) {

        this.walletService = walletService;
        this.tagService = tagService;
        this.dateHelper = dateHelper;

    }

    StatsService.prototype.getStatsForWallet = function (walletId, startDate, endDate) {

        let stats = {
            dailyChanges: {},
            dailyBalance: {},
            byTags: {}
        };

        const walletPromise = this.walletService.get(walletId).$promise;

        walletPromise.then(wallet => {

            stats.dailyChanges = this.getDailyChangesStats(wallet, startDate, endDate);
            stats.dailyBalance = this.getDailyBalanceStats(wallet, startDate, endDate);
            stats.byTags = this.getStatsByTags(wallet, startDate, endDate);

        });

        return stats;

    };

    StatsService.prototype.getDailyBalanceStats = function (wallet, startDate, endDate) {

        const today = new Date();

        const daysToSkip = this.dateHelper.getDatesBetween(endDate, today, {startDate: false, endDate: true});

        daysToSkip.reverse();

        let currentBalance = wallet.balance;

        daysToSkip.forEach(day => {

            const incomesOnDay = this.sumAmountForDay(wallet.incomes, day);
            const expensesOnDay = this.sumAmountForDay(wallet.expenses, day);

            const dayTotal = incomesOnDay - expensesOnDay;

            currentBalance -= dayTotal;

        });

        const daysToCount = this.dateHelper.getDatesBetween(startDate, endDate);
        daysToCount.reverse();

        const stats = daysToCount.map(day => {

            const incomesOnDay = this.sumAmountForDay(wallet.incomes, day);
            const expensesOnDay = this.sumAmountForDay(wallet.expenses, day);

            const dayTotal = incomesOnDay - expensesOnDay;

            const a = currentBalance;

            currentBalance -= dayTotal;

            return a;

        });

        stats.reverse();

        return buildStats(daysToCount.map(d => d.toLocaleDateString()), [wallet.name], [stats]);

    };

    StatsService.prototype.getDailyChangesStats = function (wallet, startDate, endDate) {

        const dates = this.dateHelper.getDatesBetween(startDate, endDate);

        const walletStats = dates.map(d => {

            const incomesOnDay = this.sumAmountForDay(wallet.incomes, d);
            const expensesOnDay = this.sumAmountForDay(wallet.expenses, d);

            return incomesOnDay - expensesOnDay;

        });

        return buildStats(dates.map(d => d.toLocaleDateString()), [wallet.name], [walletStats]);

    };

    StatsService.prototype.getStatsByTags = function (wallet, startDate, endDate) {

        const dates = this.dateHelper.getDatesBetween(startDate, endDate);

        let statsByTags = {
            incomes: buildStats(),
            expenses: buildStats()
        };

        const tagsPromise = this.tagService.getAll().$promise;

        tagsPromise.then(tags => {

            const countStatsForTag = (balanceChanges, tagToLookFor) => {

                return balanceChanges
                    .filter(i => this.dateHelper.isInBetween(i.whenHappened, startDate, endDate)
                        && i.tags.some(t => t.name === tagToLookFor.name))
                    .map(i => i.amount)
                    .reduce(sum, 0);

            };

            const walletStats = tags.map(t => {

                return {
                    incomes: countStatsForTag(wallet.incomes, t),
                    expenses: countStatsForTag(wallet.expenses, t)
                }

            });

            const tagLabels = tags.map(t => t.name);

            statsByTags.incomes = buildStats(tagLabels, wallet.name, walletStats.map(s => s.incomes));
            statsByTags.expenses = buildStats(tagLabels, wallet.name, walletStats.map(s => s.expenses));

        });

        return statsByTags;

    };

    StatsService.prototype.sumAmountForDay = function (balanceChanges, day) {
        return balanceChanges
            .filter(i => this.dateHelper.sameDay(i.whenHappened, day))
            .map(i => i.amount)
            .reduce(sum, 0);
    };

    StatsService.$inject = ['WalletService', 'TagService', 'DateHelper'];

    angular
        .module('money.stats')
        .service('StatsService', StatsService);

    function sum(a, b) {
        return a + b;
    }

    function buildStats(labels, series, data) {
        return {
            labels: labels || [],
            series: series || [],
            data: data || []
        };
    }

})(angular);
