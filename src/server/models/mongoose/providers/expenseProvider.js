﻿'use strict';

const NestedCollectionProvider = require('./nestedCollectionProvider');

const ExpenseProvider = NestedCollectionProvider.bind(null, [user => user.wallets, wallet => wallet.expenses]);

module.exports = ExpenseProvider;
