﻿(function (angular) {

    'use strict';

    angular
        .module('money.directives')
        .directive('openDatepickerOnClick', openDatepickerOnClick);

    function openDatepickerOnClick($parse) {

        function openDatepicker(datepickerScope) {

            datepickerScope.$apply(() => $parse('isOpen').assign(datepickerScope, 'true'));

        }

        return {
            restrict: 'A',
            link: (scope, datepicker) => datepicker.on('click', () => openDatepicker(datepicker.isolateScope()))
        };

    }

    openDatepickerOnClick.$inject = ['$parse'];

})(angular);
