﻿(function (angular) {

    'use strict';

    function buildRoute(stateName, name, link) {

        return {
            stateName: stateName,
            name: name,
            path: link
        };

    }

    angular
        .module('money.routes', [])
        .constant('ROUTES', {
            home: buildRoute('app', 'Home', '/'),
            myWallets: buildRoute('app.wallets', 'My Wallets', 'wallet/'),
            newWallet: buildRoute('app.wallets.new', 'Add New Wallet', 'new/'),
            walletBalance: buildRoute('app.wallets.balance', 'Balance For Wallet', ':walletId/balance/'),
            newIncome: buildRoute('app.wallets.newIncome', 'Add Income To Wallet', ':walletId/income/new/'),
            newExpense: buildRoute('app.wallets.newExpense', 'Add Expense To Wallet', ':walletId/expense/new/'),
            statistics: buildRoute('app.statistics', 'Statistics', 'stats')
        });

})(angular);
