﻿(function (angular) {

    'use strict';

    function balanceChangeIcon(BalanceChangeTypes) {

        return {

            restrict: 'E',
            scope: {
                type: '@'
            },
            templateUrl: 'app/wallet/management/balance-change-icon.html',
            link: scope => {

                scope.getIconClass = function () {
                    
                    if (scope.type == BalanceChangeTypes.EXPENSE.id) {
                        return 'fa-minus';
                    }

                    if (scope.type == BalanceChangeTypes.INCOME.id) {
                        return 'fa-plus';
                    }

                };

            }

        };

    };

    balanceChangeIcon.$inject = ['BalanceChangeTypes'];

    angular
        .module('money.wallet.management')
        .directive('balanceChangeIcon', balanceChangeIcon);

})(angular);
