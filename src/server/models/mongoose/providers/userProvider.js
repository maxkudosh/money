﻿'use strict';

class UserProvider {

    constructor(userModel) {
        this.userModel = userModel;
    }

    getByName(name, done) {

        return this.userModel
            .findOne({ name: name })
            .then(user => done(null, user))
            .catch(error => done(error));

    }

    getById(id, done) {

        return this.userModel
            .findById(id)
            .then(user => done(null, user))
            .catch(error => done(error));

    }

    create(name, salt, hash, done) {

        const newUser = new this.userModel({
            name: name,
            salt: salt,
            hash: hash
        });

        return newUser.save(function (error, createdUser) {

            if (error) {
                return done(error);
            }

            return done(null, createdUser);

        });

    }

}

module.exports = UserProvider;
