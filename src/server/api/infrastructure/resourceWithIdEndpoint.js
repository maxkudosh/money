﻿'use strict';

const express = require('express');

const ResourceEndpoint = require('./resourceEndpoint');

class ResourceWithIdEndpoint extends ResourceEndpoint {

    constructor(idParameter, orderedRouteParameters, provider) {

        super(orderedRouteParameters, provider);

        this.idParameter = idParameter;

    }

    getRouteValuesWithResourceId(request) {

        let routeValues = this.getRouteValues(request);

        routeValues.push(request.params[this.idParameter]);

        return routeValues;

    }

    get(req, res) {

        const routeParamsValues = this.getRouteValuesWithResourceId(req);

        return this.provider.getById.call(this.provider, (error, data) => res.status(200).json(data), ...routeParamsValues);

    }

    delete(req, res) {

        const routeParamsValues = this.getRouteValuesWithResourceId(req);

        return this.provider.delete.call(this.provider, (error) => res.sendStatus(204), ...routeParamsValues);

    }

    put(req, res) {

        const routeParamsValues = this.getRouteValuesWithResourceId(req);

        const data = req.body;

        return this.provider.update.call(this.provider, data, (error, data) => res.sendStatus(200), ...routeParamsValues);

    }

}

module.exports = ResourceWithIdEndpoint;