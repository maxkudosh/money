﻿(function (angular) {

    'use strict';

    const FA_ICON_CLASSES = {
        USD: 'fa-usd',
        BTC: 'fa-btc'
    };

    angular
        .module('money.currency.icons')
        .constant('CURRENCY_ICONS', FA_ICON_CLASSES);

})(angular);
