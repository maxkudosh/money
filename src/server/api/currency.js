﻿'use strict';

const currencyProvider = require('../models').getCurrencyProvider();

const Endpoint = require('./infrastructure/resourceEndpoint');

const currencyEndpoint = new Endpoint([], currencyProvider);

const routeBinder = require('./infrastructure/resourceEndpointBinder');

module.exports = routeBinder('/api/currency/', currencyEndpoint);