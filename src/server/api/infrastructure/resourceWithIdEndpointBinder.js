﻿'use strict';

const express = require('express');

const baseBinder = require('./resourceEndpointBinder');

function bindRoute(routeToBind, idParameter, endpoint) {

    const router = baseBinder(routeToBind, endpoint);

    const routeWithResourceId = `${routeToBind}:${idParameter}`;

    router.get(routeWithResourceId, endpoint.get.bind(endpoint));
    router.delete(routeWithResourceId, endpoint.delete.bind(endpoint));
    router.put(routeWithResourceId, endpoint.put.bind(endpoint));

    return router;

}

module.exports = bindRoute;