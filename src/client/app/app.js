(function (angular) {

    'use strict';

    function configureRoutes($stateProvider, $urlRouterProvider, $resourceProvider, ROUTES) {

        $urlRouterProvider.otherwise(ROUTES.home.path);

        $stateProvider
            .state(ROUTES.home.stateName, {
                url: ROUTES.home.path,
                templateUrl: 'app/app.html'
            });

        $resourceProvider.defaults.actions.update = { method: 'PUT' };

    }

    configureRoutes.$inject = ['$stateProvider', '$urlRouterProvider', '$resourceProvider', 'ROUTES'];

    angular
        .module('money', [
            'ngResource',
            'ui.router',
            'money.navbar',
            'money.templates',
            'money.routes',
            'money.services',
            'money.wallet',
            'money.stats'
        ])
        .config(configureRoutes);

})(angular);
