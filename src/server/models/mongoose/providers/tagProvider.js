﻿'use strict';

const NestedCollectionProvider = require('./nestedCollectionProvider');

const tagProvider = NestedCollectionProvider.bind(null, [user => user.tags]);

module.exports = tagProvider;
