﻿(function (angular) {

    'use strict';

    angular.module('money.currency', [
        'ngResource',
        'money.currency.icons'
    ]);

})(angular);