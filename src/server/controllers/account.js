﻿'use strict';

var express = require('express');
var router = express.Router();
var passport = require('passport');

var strings = require('../config/resources').strings;

var constants = require('../config/constants');

var modelFactory = require('../models/');

var user = modelFactory.getUserProvider();

var UserService = require('../services/userService');

var userService = new UserService(user);

var pathes = require('../routes/pathes');

router.get(pathes.account.signup, function (request, response) {
    return response.render('account/signup');
});

router.post(pathes.account.signup, function (request, response) {

    userService.signUpNewUser(request.body.username, request.body.password, function (error, user, message) {

        if (error) {
            return response.render('account/signup', { message: error.message });
        }

        if (!user) {
            return response.render('account/signup', { message: message });
        }

        return request.login(user, function () {

            response.cookie(constants.usernameCookieName, user.name);
            return response.redirect(pathes.app);

        });

    });

});

router.get(pathes.account.signin, function (request, response) {
    return response.render('account/signin');
});

router.post(pathes.account.signin, function (request, response, next) {

    passport.authenticate('local', function (err, user, info) {

        if (err) {
            return next(err);
        }

        if (!user) {
            return response.redirect(pathes.account.signin);
        }

        request.login(user, function () {
            response.cookie(constants.usernameCookieName, user.name);
            return response.redirect(pathes.app);
        });

    })(request, response, next);

});

router.get(pathes.account.signout, function (request, response) {
    request.logout();
    response.clearCookie(constants.usernameCookieName);
    return response.redirect(pathes.app);
});

module.exports = router;
