'use strict';

module.exports = {

    mongo: {
        uri: 'mongodb://localhost:27017/money'
    },
    sessionSecret: 'no one knows it',
    seedDB: false
};
